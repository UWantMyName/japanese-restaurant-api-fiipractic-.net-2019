﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repository_Interfaces
{
    public interface IRecipeRepository
    {
        void Create(Recipe recipe);
        void Update(Recipe recipe);
        void Delete(Recipe recipe);
        IEnumerable<Recipe> GetAll();
        Recipe GetById(int id);
    }
}
