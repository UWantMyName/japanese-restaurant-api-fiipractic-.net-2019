﻿using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Ingredient
    {
        [Key]
        public int? Id { get; set; }
        public string Name { get; set; }
        public float Quantity { get; set; } // in grams
    }
}
