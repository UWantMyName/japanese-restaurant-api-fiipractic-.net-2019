﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entities
{
    public class Recipe
    {
        [Key]
        public int? Id { get; set; }

        public string Name { get; set; }
        public float Price { get; set; }
        public virtual List<Ingredient> Ingredients { get; set; }
    }
}
