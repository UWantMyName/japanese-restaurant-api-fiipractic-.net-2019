﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repositories.DTOs;
using Repositories.Services_Interfaces;
using Services.Services;

namespace JapaneseRestaurantAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;
        private ILogger<MenuController> _logger;

        public MenuController(IMenuService menuService, ILogger<MenuController> logger)
        {
            _menuService = menuService;
            _logger = logger;
        }

        // GET: api/Menu
        [HttpGet]
        public IActionResult GetAllRecipies()
        {
            var recipies = _menuService.GetAll();
            return Ok(recipies);
        }

        // GET: api/Menu/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult GetRecipeById(int Id)
        {
            Recipe recipe = _menuService.GetById(Id);
            return Ok(recipe);
        }

        // POST: api/Menu
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(Recipe))]
        public IActionResult CreateRecipe([FromBody] RecipeDTO recipeDTO)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                Recipe newRecipe = _menuService.Create(recipeDTO);
                var URI = new Uri($"{Request.GetDisplayUrl()}/{newRecipe.Id}");
                return Created(URI, newRecipe);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        // PUT: api/Menu/5
        [HttpPut("{id}")]
        public IActionResult UpdateRecipe(int id, [FromBody] Recipe recipe)
        {
            // I need to find the recipe with the specified Id
            Recipe oldRecipe = _menuService.GetById(id);

            if (oldRecipe == null)
            {
                return NotFound($"Could not find recipe with id {id}");
            }

            // Then, I need to update the old recipe with the new one mentioned in the body
            _menuService.Update(oldRecipe, recipe);

            return NoContent();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult DeleteRecipe(int id)
        {
            // find the recipe with the Id
            Recipe recipe = _menuService.GetById(id);

            if (recipe == null)
            {
                return NotFound($"Coudl not find recipe with id {id}");
            }

            // then delete it
            _menuService.Delete(recipe);

            return NoContent();
        }
    }
}
