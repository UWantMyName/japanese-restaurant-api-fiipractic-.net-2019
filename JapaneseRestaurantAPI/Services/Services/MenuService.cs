﻿using Data.Entities;
using Data.Repository_Interfaces;
using Repositories.DTOs;
using Repositories.Services_Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Services
{
    public class MenuService : IMenuService
    {
        private readonly IRecipeRepository _recipeRepository;

        public Recipe Create(RecipeDTO recipeDTO)
        {
            var newRecipe = new Recipe()
            {
                Name = recipeDTO.Name,
                Price = recipeDTO.Price,
                Ingredients = recipeDTO.Ingredients
            };

            _recipeRepository.Create(newRecipe);
            return newRecipe;
        }

        public void Delete(Recipe recipe)
        {
            _recipeRepository.Delete(recipe);
        }

        public IEnumerable<Recipe> GetAll()
        {
            var allRecipies = _recipeRepository.GetAll();
            return allRecipies;
        }

        public Recipe GetById(int Id)
        {
            Recipe recipe = _recipeRepository.GetById(Id);
            return recipe;
        }

        public void Update(Recipe dbRecipe, Recipe recipe)
        {
            dbRecipe.Name = recipe.Name;
            dbRecipe.Price = recipe.Price;
            dbRecipe.Ingredients = recipe.Ingredients;

            _recipeRepository.Update(dbRecipe);
        }
    }
}
