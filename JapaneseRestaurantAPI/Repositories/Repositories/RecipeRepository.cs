﻿using Data.Entities;
using Data.Repository_Interfaces;
using Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Repositories.Repositories
{
    public class RecipeRepository : IRecipeRepository
    {
        private readonly Context _context;

        public RecipeRepository(Context context)
        {
            _context = context;
        }

        public void Create(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        public void Delete(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Recipe> GetAll()
        {
            return _context.Recipes.ToList();
        }

        public Recipe GetById(int id)
        {
            return _context.Recipes.SingleOrDefault(s => s.Id == id);
        }

        public void Update(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        //public void Create(Recipe recipe)
        //{
        //    _context.Recipies.Add(recipe);

        //    _context.SaveChanges();
        //}

        //public void Delete(Recipe recipe)
        //{
        //    _context.Recipies.Remove(recipe);

        //    _context.SaveChanges();
        //}

        //public IEnumerable<Recipe> GetAll()
        //{
        //    return _context.Recipies.ToList();
        //}

        //public IEnumerable<Recipe> GetRecipeFromGroup(Guid id)
        //{
        //    throw new NotImplementedException();
        //}

        //public Recipe GetById(Guid id)
        //{
        //    Recipe recipe = new Recipe();

        //    return recipe;
        //}

        //public void Update(Recipe recipe)
        //{
        //    _context.Recipies.Update(recipe);

        //    _context.SaveChanges();
        //}
    }
}
