﻿using Data.Entities;
using Repositories.DTOs;
using System.Collections.Generic;

namespace Repositories.Services_Interfaces
{
    public interface IMenuService
    {
        Recipe Create(RecipeDTO recipe);
        void Update(Recipe dbRecipe, Recipe recipe);
        void Delete(Recipe recipe);
        IEnumerable<Recipe> GetAll();
        Recipe GetById(int Id);
    }
}
