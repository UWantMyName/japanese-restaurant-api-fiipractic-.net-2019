﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Repositories.DTOs
{
    public class RecipeDTO
    {
        public string Name { get; set; }
        public float Price { get; set; }
        public List<Ingredient> Ingredients { get; set; }
    }
}
