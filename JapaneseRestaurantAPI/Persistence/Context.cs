﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options) : base(options) { }

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            SeedRecipies(modelbuilder);
            SeedIngredients(modelbuilder);
        }

        private static void SeedRecipies(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<Recipe>().HasData(
                new Recipe
                {
                    Id = 1,
                    Name = "Shoyu Ramen",
                    Price = 20
                }
            );
        }

        private static void SeedIngredients(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ingredient>().HasData(
                new Ingredient
                {
                    Id = 1,
                    Name = "Chicken",
                    Quantity = 50000
                },
                new Ingredient
                {
                    Id = 2,
                    Name = "Pork",
                    Quantity = 30000
                },
                new Ingredient
                {
                    Id = 3,
                    Name = "Beef",
                    Quantity = 10000
                }
                );
            
        }
    }
}
